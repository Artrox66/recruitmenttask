package quant.azimuth.recruitment_task.processors;

import io.pkts.packet.*;
import io.pkts.packet.impl.IPv4PacketImpl;
import io.pkts.packet.impl.MACPacketImpl;
import io.pkts.packet.impl.PCapPacketImpl;
import io.pkts.packet.impl.UdpPacketImpl;
import io.pkts.protocol.Protocol;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jmx.export.annotation.ManagedOperation;
import quant.azimuth.recruitment_task.entities.PacketEntity;
import quant.azimuth.recruitment_task.repositories.Repository;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UDPPacketProcessorTest {

    @InjectMocks
    private UDPPacketProcessor udpPacketProcessor;

    @Mock
    private Packet packet;

    @Mock
    private Repository packetRepository;

    @Mock
    private UDPPacket udpPacket;

    @Mock
    private MACPacket macPacket;

    @Mock
    private IPPacket ipPacket;

    @Before
    public void init() {
        //given
        when(udpPacket.getArrivalTime()).thenReturn(123L);
        when(udpPacket.getParentPacket()).thenReturn(ipPacket);
        when(udpPacket.getName()).thenReturn("udp");
        when(udpPacket.getDestinationPort()).thenReturn(1234);
        when(udpPacket.getSourcePort()).thenReturn(4321);
        when(ipPacket.getDestinationIP()).thenReturn("192.168.0.100");
        when(ipPacket.getSourceIP()).thenReturn("10.36.10.10");
        when(macPacket.getDestinationMacAddress()).thenReturn("00:0A:E6:3E:FD:E1");
        when(macPacket.getSourceMacAddress()).thenReturn("00:0A:E6:3E:FD:E1");

    }

    @Test
    public void shouldCallSaveMethodWhenReciveUDPPacket() {
        try {
            //given
            when(packet.getPacket(Protocol.UDP)).thenReturn(udpPacket);
            when(packet.getPacket(Protocol.ETHERNET_II)).thenReturn(macPacket);
            //when
            udpPacketProcessor.process(packet, 123L);
            //then
            verify(packetRepository, times(1)).save(any(PacketEntity.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = IOException.class)
    public void shouldThrowIOException(){

    }
}