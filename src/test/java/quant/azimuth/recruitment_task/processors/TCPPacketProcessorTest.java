package quant.azimuth.recruitment_task.processors;

import io.pkts.packet.*;
import io.pkts.protocol.Protocol;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import quant.azimuth.recruitment_task.entities.PacketEntity;
import quant.azimuth.recruitment_task.repositories.Repository;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TCPPacketProcessorTest {

    @InjectMocks
    private TCPPacketProcessor tcpPacketProcesor;

    @Mock
    private Packet packet;

    @Mock
    private Repository packetRepository;

    @Mock
    private TCPPacket tcpPacket;

    @Mock
    private MACPacket macPacket;

    @Mock
    private IPPacket ipPacket;

    @Before
    public void init() {
        //given
        when(tcpPacket.getArrivalTime()).thenReturn(123L);
        when(tcpPacket.getParentPacket()).thenReturn(ipPacket);
        when(tcpPacket.getName()).thenReturn("udp");
        when(tcpPacket.getDestinationPort()).thenReturn(1234);
        when(tcpPacket.getSourcePort()).thenReturn(4321);
        when(ipPacket.getDestinationIP()).thenReturn("192.168.0.100");
        when(ipPacket.getSourceIP()).thenReturn("10.36.10.10");
        when(macPacket.getDestinationMacAddress()).thenReturn("00:0A:E6:3E:FD:E1");
        when(macPacket.getSourceMacAddress()).thenReturn("00:0A:E6:3E:FD:E1");

    }

    @Test
    public void shouldCallSaveMethodWhenReciveTCPPacket() {
        try {
            //given
            when(packet.getPacket(Protocol.TCP)).thenReturn(tcpPacket);
            when(packet.getPacket(Protocol.ETHERNET_II)).thenReturn(macPacket);
            //when
            tcpPacketProcesor.process(packet, 123L);
            //thenprivate
            verify(packetRepository,times(1)).save(any(PacketEntity.class));
        } catch (IOException e) {
            e.printStackTrace();
        }






    }
}
