package quant.azimuth.recruitment_task.configuration;

import com.codahale.metrics.ConsoleReporter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification;
import org.springframework.data.cassandra.core.cql.keyspace.DropKeyspaceSpecification;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableCassandraRepositories
public class CassandraConfig extends AbstractCassandraConfiguration {

    @Value("${cassandra.contactpoints}")
    private String contactPoints;

    @Value("${cassandra.port}")
    private int port;

    @Value("${cassandra.keyspace}")
    private String keySpace;

    @Bean
    public CassandraClusterFactoryBean cluster() {
        CassandraClusterFactoryBean cluster =
                new CassandraClusterFactoryBean();
        cluster.setKeyspaceCreations(Arrays.asList(CreateKeyspaceSpecification.createKeyspace(keySpace)));
        cluster.setKeyspaceDrops(Arrays.asList(DropKeyspaceSpecification.dropKeyspace(keySpace)));
        cluster.setJmxReportingEnabled(false);
        cluster.setContactPoints(contactPoints);
        cluster.setPort(port);

        return cluster;
    }

    @Bean
    public ConsoleReporter reporter() {
        final ConsoleReporter reporter = ConsoleReporter.forRegistry(Objects.requireNonNull(cluster().getObject()).getMetrics().getRegistry())
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(1, TimeUnit.MINUTES);
        return reporter;
    }

    @Override
    protected String getKeyspaceName() {
        return keySpace;
    }


    @Override
    protected String getContactPoints() {
        return contactPoints;
    }

    @Override
    protected int getPort() {
        return port;
    }

    @Override
    public String[] getEntityBasePackages() {
        return new String[]{"quant.azimuth"};
    }

    @Override
    public SchemaAction getSchemaAction() {
        return SchemaAction.CREATE_IF_NOT_EXISTS;
    }

    @Scheduled(cron = "${metrics.cron}")
    public void logPerformanceMetrics() {
        reporter().report();
    }
}
