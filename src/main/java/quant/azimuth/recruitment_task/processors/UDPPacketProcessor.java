package quant.azimuth.recruitment_task.processors;

import io.pkts.packet.MACPacket;
import io.pkts.packet.Packet;
import io.pkts.packet.UDPPacket;
import io.pkts.protocol.Protocol;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import quant.azimuth.recruitment_task.entities.PacketEntity;
import quant.azimuth.recruitment_task.repositories.Repository;

import java.io.IOException;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
@Component
public class UDPPacketProcessor implements PacketProcessor {

    Repository packetRepository;

    @Override
    public void process(Packet packet, long threadId) throws IOException {
        UDPPacket udpPacket = (UDPPacket) packet.getPacket(Protocol.UDP);
        MACPacket macPacket = (MACPacket) packet.getPacket(Protocol.ETHERNET_II);
        PacketEntity packetEntity = PacketEntity.builder()
                .id(UUID.randomUUID())
                .arrivalTime(udpPacket.getArrivalTime())
                .destinationIp(udpPacket.getParentPacket().getDestinationIP())
                .sourceIP(udpPacket.getParentPacket().getSourceIP())
                .sourceMac(macPacket.getSourceMacAddress())
                .destinationMac(macPacket.getDestinationMacAddress())
                .sourcePort(udpPacket.getSourcePort())
                .destinationPort(udpPacket.getDestinationPort())
                .ethernetType(udpPacket.getName()).build();
        log.debug("Thread #" + threadId + "insert packet" + packetEntity.getId());
        packetRepository.save(packetEntity);
    }
}
