package quant.azimuth.recruitment_task.processors;

import io.pkts.packet.MACPacket;
import io.pkts.packet.Packet;
import io.pkts.packet.TCPPacket;
import io.pkts.protocol.Protocol;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import quant.azimuth.recruitment_task.entities.PacketEntity;
import quant.azimuth.recruitment_task.repositories.Repository;

import java.io.IOException;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
@Component
public class TCPPacketProcessor implements PacketProcessor {

    Repository packetRepository;

    @Override
    public synchronized void process(Packet packet, long threadId) throws IOException {
        TCPPacket tcpPacket = (TCPPacket) packet.getPacket(Protocol.TCP);
        MACPacket macPacket = (MACPacket) packet.getPacket(Protocol.ETHERNET_II);

        PacketEntity packetEntity = PacketEntity.builder()
                .id(UUID.randomUUID())
                .arrivalTime(tcpPacket.getArrivalTime())
                .destinationIp(tcpPacket.getParentPacket().getDestinationIP())
                .sourceIP(tcpPacket.getParentPacket().getSourceIP())
                .sourceMac(macPacket.getSourceMacAddress())
                .destinationMac(macPacket.getDestinationMacAddress())
                .sourcePort(tcpPacket.getSourcePort())
                .destinationPort(tcpPacket.getDestinationPort())
                .ethernetType(tcpPacket.getName()).build();
        log.debug("Thread #" + threadId + "insert packet" + packetEntity.getId());
        packetRepository.save(packetEntity);
    }
}
