package quant.azimuth.recruitment_task.processors;

import com.datastax.driver.core.Session;
import io.pkts.packet.Packet;
import org.springframework.data.cassandra.core.AsyncCassandraTemplate;

import java.io.IOException;

public interface PacketProcessor {
    void process(Packet packet, long threadId) throws IOException;
}
