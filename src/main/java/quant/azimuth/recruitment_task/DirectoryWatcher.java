package quant.azimuth.recruitment_task;

import com.codahale.metrics.ConsoleReporter;
import com.datastax.driver.core.Session;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import quant.azimuth.recruitment_task.processors.TCPPacketProcessor;
import quant.azimuth.recruitment_task.processors.UDPPacketProcessor;
import quant.azimuth.recruitment_task.repositories.PacketRepository;
import quant.azimuth.recruitment_task.repositories.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static quant.azimuth.recruitment_task.factories.DirectoryWatcherFactory.createWatchService;

@Component
public class DirectoryWatcher {

    @Value("${pcap.directory}")
    private String directory;

    private Repository packetRepository;

    public DirectoryWatcher(Repository packetRepository, TCPPacketProcessor tcpPacketProcessor, UDPPacketProcessor udpPacketProcessor) {
        this.packetRepository = packetRepository;
        this.tcpPacketProcessor = tcpPacketProcessor;
        this.udpPacketProcessor = udpPacketProcessor;
    }

    private TCPPacketProcessor tcpPacketProcessor;

    private UDPPacketProcessor udpPacketProcessor;

    @PostConstruct
    public synchronized void  init() throws IOException, InterruptedException {
        WatchService watchService = createWatchService(directory);
        WatchKey watchKey;
        Set<Path> files = new HashSet<>();
        while ((watchKey = watchService.take()) != null) {
            Thread.sleep(10000);
            for (WatchEvent<?> event : watchKey.pollEvents()) {
                files.add((Path) event.context());
            }
            files.forEach( x -> {
                ExecutorService service = Executors.newFixedThreadPool(1);
                service.submit(new FileReader(x,directory,tcpPacketProcessor,udpPacketProcessor));
            });
            files.clear();
            watchKey.reset();
        }
        files.clear();
    }

}

