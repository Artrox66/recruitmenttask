package quant.azimuth.recruitment_task.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table("packet")
@AllArgsConstructor
@Builder
@Getter
public class PacketEntity {

    @PrimaryKey
    private final UUID id;

    private final Long arrivalTime;

    private final String destinationMac;

    private final String sourceMac;

    private final String ethernetType;

    private final String sourceIP;

    private final String destinationIp;

    private final int sourcePort;

    private final int destinationPort;
}
