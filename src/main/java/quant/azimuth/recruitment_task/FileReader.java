package quant.azimuth.recruitment_task;

import io.pkts.Pcap;
import io.pkts.protocol.Protocol;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import quant.azimuth.recruitment_task.processors.TCPPacketProcessor;
import quant.azimuth.recruitment_task.processors.UDPPacketProcessor;
import quant.azimuth.recruitment_task.repositories.Repository;

import java.io.IOException;
import java.nio.file.Path;

@Slf4j
@AllArgsConstructor
public class FileReader implements Runnable {

    private Path file;
    private String directory;
    private TCPPacketProcessor tcpPacketProcessor;
    private UDPPacketProcessor udpPacketProcessor;

    @Override
    public void run() {
        long threadId = Thread.currentThread().getId();
        Pcap pcap = null;
        try {
            log.debug("Thread #" + threadId + "started for file" + file.toString());

            pcap = Pcap.openStream(directory + "/" + file.toString());
            pcap.loop(packet -> {
                if (packet.hasProtocol(Protocol.TCP)) {
                    tcpPacketProcessor.process(packet, threadId);
                } else if (packet.hasProtocol(Protocol.UDP)) {
                    udpPacketProcessor.process(packet, threadId);
                }
                return true;
            });
        } catch (IOException e) {
            log.debug("Failed to start #Thread " + threadId + "could not read file " + file.toString());
            e.printStackTrace();
        } finally {
            if (pcap != null) pcap.close();
        }
    }
}
