package quant.azimuth.recruitment_task.repositories;

import quant.azimuth.recruitment_task.entities.PacketEntity;


public interface Repository {
    void save(PacketEntity packetEntity);
}
