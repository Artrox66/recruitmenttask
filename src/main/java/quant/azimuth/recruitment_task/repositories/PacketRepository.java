package quant.azimuth.recruitment_task.repositories;

import com.datastax.driver.core.Session;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.AsyncCassandraTemplate;
import quant.azimuth.recruitment_task.entities.PacketEntity;

@AllArgsConstructor
@org.springframework.stereotype.Repository
public class PacketRepository implements Repository {

    final
    Session session;

    @Override
    public void save(PacketEntity packetEntity) {
        AsyncCassandraTemplate asyncTemplate = new AsyncCassandraTemplate(session);
        asyncTemplate.insert(packetEntity);
    }
}
