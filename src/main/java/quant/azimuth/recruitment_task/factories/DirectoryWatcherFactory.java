package quant.azimuth.recruitment_task.factories;

import java.io.IOException;
import java.nio.file.*;

public class DirectoryWatcherFactory {

    public static WatchService createWatchService(String path) throws IOException {
        WatchService watchService = FileSystems.getDefault().newWatchService();
        Path directory = Paths.get(path);
        directory.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
        return watchService;
    }

}
